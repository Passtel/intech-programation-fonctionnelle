let board = [
    [null, null, null],
    [null, null, null],
    [null, null, null]
];
let player = "X";
let points = {
    "X": 0,
    "O": 0,
};

function arrayGenerator(board) {
    let table = document.createElement("table");
    board.forEach((line, j) => {
        let row = document.createElement("tr");
        line.forEach((x, i) => {
            let cell = document.createElement("td");
            switch (x) {
                case null:
                    cell.style.backgroundColor = "yellow";
                    break;
                case 'X':
                    cell.style.backgroundColor = "blue";
                    break;
                case 'O':
                    cell.style.backgroundColor = "red";
                    break;
                default:
            }
            row.appendChild(cell);
        });
        table.appendChild(row);
    })
    document.body.appendChild(table);
}

function play(row, col) {
    if (board[row][col] === null) {
        board[row][col] = player;
        checkWin(row, col);
        player = player === "X" ? "O" : "X";
    }
    arrayGenerator(board);
}

function checkWin(row, col) {
    let symbol = board[row][col];
    let win =
        (board[0][col] === symbol && board[1][col] === symbol &&
            board[2][col] === symbol) ||
        (board[row][0] === symbol &&
            board[row][1] === symbol &&
            board[row][2] === symbol) ||
        (row === col &&
            board[0][0] === symbol &&
            board[1][1] === symbol &&
            board[2][2] === symbol) ||
        (row + col === 2 &&
            board[0][2] === symbol &&
            board[1][1] === symbol &&
            board[2][0] === symbol);

    if (win) {
        points[symbol]++;
        console.log(`${symbol} a gagné la partie !`);
    }
}

play(0, 0);
play(1, 1);
play(0, 1);
play(1, 2);
play(0, 2);
console.log(points);