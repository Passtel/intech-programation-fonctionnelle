function drawBar(sum, segment) {
    let bar = document.createElement("div");
    bar.style.width = "100%";
    bar.style.height = "20px";
    bar.style.border = "1px solid black";

    let coloredSegment = document.createElement("div");
    coloredSegment.style.width = (segment / sum) * 100 + "%";
    coloredSegment.style.height = "100%";
    coloredSegment.style.backgroundColor = "blue";

    bar.appendChild(coloredSegment);

    return bar;
}

const draw = drawBar(15, 10);

const container = document.getElementById("draw-container");
document.body.appendChild(draw);